##################
# Importe
##################

from flask import Flask, request, session, redirect, render_template
from time import sleep
import hashlib
import json
import sqlite3
import Xlib
import Xlib.display
import pulsectl
import subprocess


###################
# Flask
###################

# Applikation definieren
########################

app = Flask(__name__)
app.secret_key = "68d10aed221d9ceb65d9fcb6a3fed03615ca353ff691ce5ee1c558d0cf8f798a"

# Routes
#########

# index/document root
@app.route('/')
def root():
	return redirect('/index')


@app.route('/index')
def static_proxy():
	#print(session)
	if 'loggedin' in session:
		#return app.send_static_file(path)
		serial_data = osif.serialize_data()
		return render_template("skelett.html", data=json.loads(serial_data))
	else:
		return redirect('/login')


# assets ohne Login verfügar
@app.route('/assets/<path:path>')
def asset_proxy(path):
	return app.send_static_file('assets/' + path)


# Login-Funktion
@app.route('/login', methods=['POST', 'GET'])
def login():
	if request.method == 'GET':
		return app.send_static_file('login.html')
	elif request.method == 'POST':
		if dbif.check_pwd(request.form['pwd']):
			session['loggedin'] = True
			return redirect('/index')
		else:
			sleep(1)
			return '<script type="text/javascript">alert("Password incorrect"); window.history.back();</script>'


# Logout-Funktion
@app.route('/logout')
def logout():
	session.clear()
	return redirect('/login')


# API-Funktion
@app.route('/api/<path:path>', methods=['POST', 'GET'])
def api_function(path):

	if 'loggedin' in session and session['loggedin'] is True:

		cmd = path.split("/")


		# Daten von Python -> JS
		if cmd[0] == 'get':
			return osif.serialize_data()

		# Daten von JS -> Python
		elif cmd[0] == 'post':
			#return cmd[1]

			#Lautstärke ändern
			if cmd[1] == 'sound':
				if cmd[2] == 'changeVol':
					osif.set_volume(cmd[3],cmd[4],cmd[5])

				elif cmd[2] == 'setMute':
					osif.set_mute(cmd[3],cmd[4],cmd[5])
				return ""

			#System-Befehle
			elif cmd[1] == 'system':
				if cmd[2] == 'shutdown':
					subprocess.call('sudo shutdown -h now', shell=True)
					print("I should shutdown")
					return "Message received, will shut down."
				elif cmd[2] == 'reboot':
					subprocess.call('sudo reboot', shell=True)
					print("I should reboot")
					return "Message received, will reboot."
				elif cmd[2] == 'killApp':
					#TODO: kill App-Befehl
					print("I should kill the Application")
					return "Message received, will kill Application."

			#Passwort ändern
			elif cmd[1] == 'pwd':
				if dbif.receive_pwd(request.form):
					return "<span class='fbOk'>Data received, changing the locks...</span>"
				else:
					return "<span class='fbError'>Something went wrong.</span>"

			# URL ungültig
			else:
				return "<span class='fbError'>URL invalid</span>"

		# URL ungültig
		else:
			return "<span class='fbError'>URL invalid</span>"

	# nicht eingeloggt:
	else:
		return "access denied"

	'''
			# Sonnen-Daten
			if cmd[1] == 'sun':
				dbHandle.receive_sun(request.form)
				#print(dict(request.form))
				return "<span class='fbOk'>Data received, telling Helios...</span>"

			# Sonnen-Daten
			if cmd[1] == 'solarwind':
				dbHandle.receive_solarwind(request.form, cmd[2])
				#print(dict(request.form))
				return "<span class='fbOk'>Data received, telling Helios...</span>"

			# Wetter-Daten
			elif cmd[1] == 'rain':
				dbHandle.receive_rain(request.form, cmd[2])
				return "<span class='fbOk'>Data received, changing the weather...</span>"

			# Hardware-Daten
			elif cmd[1] == 'hw':
				#print(request.form)
				dbHandle.receive_hw(request.form, cmd[2], cmd[3])
				return "<span class='fbOk'>Data received, rewiring stuff...</span>"
	'''



################
# SQL-Interface
################
class DBInterface:

	def __init__(self, db):
		self.db = db
		self.con = db.cursor()


	def receive_pwd(self, form_data):
		old_hash = str(hashlib.sha256(form_data['old_pwd'].encode()).hexdigest())
		self.con.execute('SELECT passwdHash, id FROM passwds WHERE passwdHash=?', (old_hash,))
		db_data = self.con.fetchone()
		try:
			id_to_change = db_data['id']
			try:
				if db_data['passwdHash'] == old_hash and form_data['new_pwd_1'] == form_data['new_pwd_2'] and len(form_data['new_pwd_1']) > 4:
					new_hash = str(hashlib.sha256(form_data['new_pwd_1'].encode()).hexdigest())
					self.con.execute("UPDATE passwds SET passwdHash=? WHERE id=?", (new_hash, id_to_change))
					self.db.commit()
					return True
				else:
					print("Wrong: Password Check")
					return False
			except TypeError:
				print("Wrong: try Password Check")
				return False
		except TypeError or KeyError:
			print("Wrong: access db_data id")
			return False


	def check_pwd(self, pwd_to_chk):
		pwd_hash = str(hashlib.sha256(pwd_to_chk.encode()).hexdigest())
		self.con.execute('SELECT passwdHash FROM passwds WHERE passwdHash=?', (pwd_hash,))
		try:
			if self.con.fetchone()[0] == pwd_hash:
				return True
			else:
				return False
		except TypeError:
			return False


#####################
# OS Interface
#####################
class OSInterface:

	def __init__(self):
		self.re_dict = {}
		self.pulse = pulsectl.Pulse()


	def serialize_data(self):
		self.get_volumes('sink', self.pulse.sink_list())
		self.get_volumes('source', self.pulse.source_list())
		print(self.re_dict)
		return json.dumps(self.re_dict)


	def get_resolutions(self):
		display = Xlib.display.Display(':0')
		print (display.screen_count())


	def get_volumes(self, key, pulse_list):
		if not 'pulse' in self.re_dict.keys():
			self.re_dict["pulse"] = {}
		self.re_dict["pulse"][key] = []
		for i in range(len(pulse_list)):
			self.re_dict["pulse"][key].insert(i, {'index': i,'description': pulse_list[i].description, 'volume': int(pulse_list[i].volume.values[0]*100), 'muted': pulse_list[i].mute})


	def set_volume(self, direction, list_index, volume):
		list_index = int(list_index)
		if direction == 'sink':
			device_to_change = self.pulse.sink_list()[list_index]
		elif direction == 'source':
			device_to_change = self.pulse.source_list()[list_index]
		self.pulse.volume_set_all_chans(device_to_change, float(volume)/100)


	def set_mute(self, direction, list_index, mute_value):
		list_index = int(list_index)
		mute_value = bool(int(mute_value))
		if direction == 'sink':
			device_to_change = self.pulse.sink_list()[list_index]
		elif direction == 'source':
			device_to_change = self.pulse.source_list()[list_index]
		self.pulse.mute(device_to_change, mute_value)


# Alles ausführen
#################

# Datenbank initialisieren
sql = sqlite3.connect('interface.db')
sql.row_factory = sqlite3.Row

# Interface-Objekte für Flask
dbif = DBInterface(sql)
osif = OSInterface()

# Testen
osif.serialize_data()

# Server starten
app.run(host='0.0.0.0')


