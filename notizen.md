##Beispiel für CLI-VLC-Stream:

Codec: OGG

###Video
Codec: Theodora

Bitrate: 800kb/s

fps: 15fps

Audio: 

###Audio

Codec: Vorbis

Bitrate: 128kb/s

samplerate: 48k

###Befehl:

    cvlc v4l2:///dev/video0 --sout '#transcode{vcodec=theo,vb=800,fps=15,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'

###mit Multithread:
    cvlc v4l2:///dev/video0 --sout '#transcode{vcodec=theo,threads=4,vb=800,fps=15,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'

###ziemlich gutes Ergebnis bis jetzt (latency < 2s) - aber ohne Sound
    cvlc v4l2:///dev/video0 --sout '#transcode{vcodec=theo,threads=2,vb=1400,fps=20,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'


###Pulseaudio-Experiment
    cvlc v4l2:///dev/video0 --input-slave pulse://alsa_input.pci-0000_00_1b.0.analog-stereo --sout '#transcode{vcodec=theo,threads=2,vb=1400,fps=20,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'

    cvlc v4l2:///dev/video0 :input-slave=pulse://alsa_input.pci-0000_00_1b.0.analog-stereo --sout '#transcode{vcodec=theo,threads=2,vb=1400,fps=20,scale=Auto,acodec=vorb,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'

    cvlc v4l2:///dev/video0 :input-slave=pulse://alsa_input.pci-0000_00_1b.0.analog-stereo --sout '#transcode{vcodec=theo,threads=2,vb=1400,fps=20,scale=Auto,acodec=opus,ab=128,channels=2,samplerate=48000}:std{access=shout,mux=ogg,dst=source:melike@ice.midi.xyz:8000/client1.ogg}'
