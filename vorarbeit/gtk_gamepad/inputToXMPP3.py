
import sleekxmpp
from inputs import get_gamepad

buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
axesList = ["ABS_X", "ABS_Y"]
gpInt = 0

class SendMsgBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		#self.recipient = recipient
		self.add_event_handler("session_start", self.start, threaded=True)


	def start(self, event):
		self.send_presence()
		self.get_roster()


	def sendData(self, gpRecipient, gpMsg):
		self.connect()
		self.send_message(gpRecipient, gpMsg, 'chat')
		self.process()


	def cutCon(self):
		self.disconnect(wait=True)


if __name__ == '__main__':
	
	while 1:
		events = get_gamepad()
		buttonCounter = 0
		relevantChanges = 0
		
		for event in events:
			for eachButton in buttonList:
				if event.ev_type == "Key" and event.code == eachButton:
					relevantChanges = relevantChanges + 1
					if event.state == 1 and gpInt & 2**buttonCounter == 0:
						gpInt = gpInt + 2**buttonCounter
					elif gpInt & 2**buttonCounter > 0:
						gpInt = gpInt - 2**buttonCounter
				
				buttonCounter = buttonCounter + 1
				
			for eachAxe in axesList:
				if event.ev_type == "Absolute" and event.code == eachAxe:
					relevantChanges = relevantChanges + 1
					if event.state < 100 and gpInt & 2**buttonCounter == 0:
						gpInt = gpInt + 2**buttonCounter
					elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
						gpInt = gpInt + 2**(buttonCounter + 1)
					elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
						gpInt = gpInt - 2**buttonCounter
					elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
						gpInt = gpInt - 2**(buttonCounter + 1)
					else:
						pass
				
				buttonCounter = buttonCounter + 2
				
		if(relevantChanges > 0):
			xmpp = SendMsgBot("ice@ice.midi.xyz", "me_like!")
			xmpp.sendData("ice@ice.midi.xyz", str(gpInt))
			print(gpInt)
