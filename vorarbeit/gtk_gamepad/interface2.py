'''
BIBLIOTHEKEN IMPORTIEREN
gi: Grafisches Interface
GTK: Grafisches Toolkit
Gdk: Offenbar nötig für CSS-Import
GObject: nötig, um Event Listeners an Main Window anzuhängen
Inputs: Gamepad lesen
threading: Event Listener in threads abschieben
'''

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject
from inputs import get_gamepad
import threading


"""
Gamepad Listener Thread, der Signals emitiert, auf die Event Listeners wiederum reagieren
GObject-Klasse übergeben, damit Event Listeners mit emit() definiert werden können
"""
class gpListener(GObject.GObject):
	"""
	Welche Events können emitiert werden und welche Daten werden mitgesendet
	Erstes Argument macht das Ding zu einem Signal, das abgefangen werden kann
	Zweites Argument muss offenbar None sein (Typ des Signals, scheinbar)
	Drittes Argument ist eine Liste der Argumente, die der Callback-Funktion übergeben werden
	"""
	__gsignals__ = {
		'relevantButton': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
	}

	"""
	Die Klasse wird initialisiert, indem ihre übergeordnete Klasse GObject initialisiert wird
	"""
	def __init__(self):
		GObject.GObject.__init__(self)

	"""
	Hier wird dann effektiv was gemacht: Der Loop des Listeners, der schliesslich die Signale abfeuert mit emit()
	Erstes Argument ist der Name des Signals, das übergeben wird
	Danach die Argumente, die der Callback-Funktion übergeben werden
	"""
	def startListening(self):
		while 1:
			events = get_gamepad()
			for event in events:
				if event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
					self.emit("relevantButton", "start") #Signal, das abgefeuert wird + die Argumente, die es mitliefern muss
				elif event.ev_type == "Absolute" and event.code == "ABS_Y":
					if event.state < 100:
						self.emit("relevantButton", "up")
					elif event.state > 200:
						self.emit("relevantButton", "down")


"""
Die GTK-Klasse, die das effektive Fenster zeichnet und an der alle Event Listeners hängen
"""
class ButtonWindow(Gtk.Window):

	def __init__(self):

		#übergeordnete Klasse (GTK) wird initialisiert
		Gtk.Window.__init__(self, title="Button Demo")

		#No idea, wird schon stimmen - Layout?
		self.set_border_width(10)
		hbox = Gtk.Box(spacing=6)
		self.add(hbox)

		#Nötige Variabeln der Instanz
		self.anzClients = 2 #Wieviele Clients gibt's, wieviele Buttons werden gezeichnet
		self.btnList = []  #Hierrein werden die Button-Objekte gespeichert
		self.selectedButton = -1 #Speicher, welcher Button im Moment durch das Gamepad selektioniert wurde

		#Die Buttons zeichnen und in btnList speichern
		for nBtn in range(0, self.anzClients):
			self.btnList.append(Gtk.Button("Client %d" % (nBtn + 1))) #Text draufschreiben
			self.btnList[nBtn].connect("clicked", self.printBtnNr, nBtn) #Event Listener/auszuführende Funktion anhängen
			hbox.pack_start(self.btnList[nBtn], True, True, 0) #Keine Ahnung, was das macht


		"""
		Hier wird der Thread, der das Gamepad checked, gestartet und der Event Listener definiert
		Das Objekt wird initialisiert, dann der Event Listener an die zu emitierenden Signals gehängt
		Schliesslich wird jene Methode gestartet, die den eigentlichen Loop und die emit-Funktionen enthält
		"""
		listener = gpListener()
		listener.connect("relevantButton", self.buttonDetected) #Event Listener anhängen: Signal-Name, Callback -> die Argumente, die dem Callback übergeben werden, sind definiert durch die emit-Funktion
		gpThread = threading.Thread(target=listener.startListening, args=()) #Aus der Methode, die den Loop enthält, wird ein Thread gemacht
		gpThread.start() #Der Thread/Loop wird gestartet


	#Diese Funktion wird ausgeführt, wenn ein Button gedrückt wird oder per Gamepad ausgewählt wird - Hauptsächlich zur Überprüfung
	def printBtnNr(self, button, clientNr):
		print("called Client %d" % int(clientNr + 1))


	#Das Markieren der Buttons. CSS-Klassen entfernen und anhängen, nur optisch relevant
	def markBtn(self, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	"""
	Die Callback-Funktion der Gamepad-Events
	So wie das definierte Signal drei Argumente hat, muss auch das Callback offenbar drei Argumente haben
	data1 ist mir überhaupt nicht klar, was das soll
	data2 ist schliesslich der oben definierte String, der den Button identifiziert
	"""
	def buttonDetected(self, data1, data2):
		print("button detected!")
		print(data2)
		if data2 == "down":
			if self.selectedButton < self.anzClients - 1:
				self.selectedButton += 1
			self.markBtn(self.selectedButton)
		elif data2 == "up":
			if self.selectedButton > 0:
				self.selectedButton -= 1
			self.markBtn(self.selectedButton)
		elif data2 == "start":
			self.printBtnNr(None, self.selectedButton) #erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist


	def on_close_clicked(self, button):
		print("Closing application")
		Gtk.main_quit()


"""
CSS-File integrieren
"""
cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)

"""
GTK-Applikation starten
"""
win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
