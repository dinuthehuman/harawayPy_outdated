from inputs import get_gamepad
import sleekxmpp
import sys

class chatBot(sleekxmpp.ClientXMPP):
	
	def __init__(self, jid, password):
		print("starte Objekt...")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self.jid = jid
		self.add_event_handler("session_start", self.start,threaded=True)
	
	def start(self, event):
		self.send_presence()
		self.get_roster()
		print("XMPP am Start")
		
	def transmitData(msg):
		self.send_message(mto=self.jid,
                          mbody=msg,
                          mtype='chat')
		
		


buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
axesList = ["ABS_X", "ABS_Y"]
gpInt = 0

xmpp = chatBot('admin@ice.midi.xyz', 'Iot_13347')
xmpp.connect()

while 1:
	events = get_gamepad()
	buttonCounter = 0
	relevantChanges = 0
	
	for event in events:
		for eachButton in buttonList:
			if event.ev_type == "Key" and event.code == eachButton:
				relevantChanges = relevantChanges + 1
				if event.state == 1 and gpInt & 2**buttonCounter == 0:
					gpInt = gpInt + 2**buttonCounter
				elif gpInt & 2**buttonCounter > 0:
					gpInt = gpInt - 2**buttonCounter
			
			buttonCounter = buttonCounter + 1
			
		for eachAxe in axesList:
			if event.ev_type == "Absolute" and event.code == eachAxe:
				relevantChanges = relevantChanges + 1
				if event.state < 100 and gpInt & 2**buttonCounter == 0:
					gpInt = gpInt + 2**buttonCounter
				elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
					gpInt = gpInt + 2**(buttonCounter + 1)
				elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
					gpInt = gpInt - 2**buttonCounter
				elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
					gpInt = gpInt - 2**(buttonCounter + 1)
				else:
					pass
			
			buttonCounter = buttonCounter + 2
			
	if(relevantChanges > 0):
		print(gpInt)
		xmpp.transmitData(gpInt)
		xmpp.process()

