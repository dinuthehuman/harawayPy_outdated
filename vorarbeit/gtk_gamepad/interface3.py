
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject
from inputs import get_gamepad
import threading


"""
Global Scope Variabeln
Zustand, in dem sich das Programm grad befindet
"""
dispMenu = True
isConnected = False
dispOverlay = False
rolle = None


"""
Gamepad Listener
"""
class gpListener(GObject.GObject):


	__gsignals__ = {
		'relevantButton': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
	}


	def __init__(self):
		GObject.GObject.__init__(self)


	def startListening(self):
		while 1:
			events = get_gamepad()
			for event in events:

				# Event emitter für Client Menu Führung
				if dispMenu:
					if event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start") #Signal, das abgefeuert wird + die Argumente, die es mitliefern muss
					elif event.ev_type == "Absolute" and event.code == "ABS_Y":
						if event.state < 100:
							self.emit("relevantButton", "up")
						elif event.state > 200:
							self.emit("relevantButton", "down")

				# Event emitter für Client Interface
				elif not dispMenu:

					if isConnected:
						pass #HIER DANN GAMEPAD XMPP EINBAUEN!

					# Overlay triggern
					if not dispOverlay and event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start")
					# Zurück zum Menu
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_THUMB" and event.state == 1:
						self.emit("relevantButton", "rot")
					# Overlay wieder verstecken
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_TOP" and event.state == 1:
						self.emit("relevantButton", "grun")



"""
Die GTK-Klasse, die das effektive Fenster zeichnet und an der alle Event Listeners hängen
"""
class ButtonWindow(Gtk.Window):

	def __init__(self):

		#übergeordnete Klasse (GTK) wird initialisiert
		Gtk.Window.__init__(self, title="Button Demo")

		#GTK global
		self.set_border_width(10)

		#GTK Menuführung
		self.menubox = Gtk.Box(spacing=6)
		self.add(self.menubox)
		self.anzClients = 2 #Wieviele Clients gibt's, wieviele Buttons werden gezeichnet
		self.btnList = []  #Hierrein werden die Button-Objekte gespeichert
		self.selectedButton = -1 #Speicher, welcher Button im Moment durch das Gamepad selektioniert wurde

		#Die Buttons zeichnen und in btnList speichern
		for nBtn in range(0, self.anzClients):
			self.btnList.append(Gtk.Button("Client %d" % (nBtn + 1))) #Text draufschreiben
			self.btnList[nBtn].connect("clicked", self.dispIF, "client%d" % nBtn) #Event Listener/auszuführende Funktion anhängen
			self.menubox.pack_start(self.btnList[nBtn], True, True, 0) #Keine Ahnung, was das macht

		#GTK Interface
		#self.ifbox = Gtk.Box(spacing=6)
		#self.add(self.ifbox)
		self.ifTestLabel = Gtk.Label("Hier VLC imaginieren")
		self.ifOverlay = Gtk.Label("Wirklich beenden? rot: JA! // grün: NEIN!")
		self.menubox.pack_start(self.ifTestLabel, True, True, 0)
		self.menubox.pack_start(self.ifOverlay, True, True, 0)

		listener = gpListener()
		listener.connect("relevantButton", self.buttonDetected) #Event Listener anhängen: Signal-Name, Callback -> die Argumente, die dem Callback übergeben werden, sind definiert durch die emit-Funktion
		gpThread = threading.Thread(target=listener.startListening, args=()) #Aus der Methode, die den Loop enthält, wird ein Thread gemacht
		gpThread.start() #Der Thread/Loop wird gestartet


	#Diese Funktion wird ausgeführt, wenn ein Button gedrückt wird oder per Gamepad ausgewählt wird - Hauptsächlich zur Überprüfung
	def printBtnNr(self, button, clientNr):
		print("called Client %d" % int(clientNr + 1))


	"""
	Gamepad Listener Callback, an GTK angehänt
	"""
	def buttonDetected(self, data1, data2):
		print("button detected!")
		print(data2)

		if dispMenu:
			if data2 == "down":
				if self.selectedButton < self.anzClients - 1:
					self.selectedButton += 1
				self.markBtn(self.selectedButton)
			elif data2 == "up":
				if self.selectedButton > 0:
					self.selectedButton -= 1
				self.markBtn(self.selectedButton)
			elif data2 == "start" and self.selectedButton >= 0:
				self.printBtnNr(None, self.selectedButton) #erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist
				self.dispIF(None, "client%d" % self.selectedButton) #erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist

		elif not dispMenu:
			if data2 == "start" and not dispOverlay:
				self.dispOverlay(True)
			elif data2 == "grun" and dispOverlay:
				self.dispOverlay(False)
			elif data2 == "rot" and dispOverlay:
				self.dispMenu()


	"""
	Grafische Funktionen, Steuerung der Menuführung
	"""
	#Das Markieren der Buttons. CSS-Klassen entfernen und anhängen, nur optisch für Menuführung relevant
	def markBtn(self, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	#Client Interface ausblenden, Menu anzeigen
	def dispMenu(self):
		global rolle, dispMenu, dispOverlay, isConnected
		rolle = None
		dispMenu = True
		dispOverlay = False
		isConnected = False
		self.ifTestLabel.hide()
		self.ifOverlay.hide()
		for btn in self.btnList:
			btn.show()


	#Menu ausblenden, Client Interface anzeigen
	def dispIF(self, button, setRolle):
		global rolle, dispMenu
		rolle = setRolle
		print("Rolle gewählt: %s" % rolle)
		dispMenu = False
		for btn in self.btnList:
			btn.hide()
		self.ifTestLabel.show()


	#Quit Overlay anzeigen
	def dispOverlay(self, setOverlay):
		global dispOverlay
		if setOverlay:
			dispOverlay = True
			self.ifOverlay.show()
		else:
			dispOverlay = False
			self.ifOverlay.hide()


	def on_close_clicked(self, button):
		print("Closing application")
		Gtk.main_quit()



"""
CSS-File integrieren
"""
cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)


"""
GTK-Applikation starten
"""
win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
win.dispMenu()
Gtk.main()
