
# Bibliotheken importieren
##########################

from multiprocessing import Process, Queue
import sleekxmpp
from inputs import get_gamepad
import time



# Globale Variabeln
###################

gpInt = 0



# XMPP Klasse
#############

class SendMsgBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self.add_event_handler("session_start", self.start, threaded=True)


	def start(self, event):
		self.send_presence()
		self.get_roster()


	def sendData(self, gpRecipient, gpMsg):
		self.connect()
		self.send_message(gpRecipient, gpMsg, 'chat')
		self.process()


	def cutCon(self):
		self.disconnect(wait=True)



# Gamepad Funktionen
####################

def serializeGP():
	
	global gpInt
	buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
	axesList = ["ABS_X", "ABS_Y"]
	buttonCounter = 0
	relevantChanges = 0
	
	events = get_gamepad()
	
	for event in events:
		for eachButton in buttonList:
			if event.ev_type == "Key" and event.code == eachButton:
				relevantChanges += 1
				if event.state == 1 and gpInt & 2**buttonCounter == 0:
					gpInt += 2**buttonCounter
				elif gpInt & 2**buttonCounter > 0:
					gpInt -= 2**buttonCounter
				else:
					pass
			buttonCounter += 1
			
		for eachAxe in axesList:
			if event.ev_type == "Absolute" and event.code == eachAxe:
				relevantChanges += 1
				if event.state < 100 and gpInt & 2**buttonCounter == 0:
					gpInt += 2**buttonCounter
				elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
					gpInt += 2**(buttonCounter + 1)
				elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
					gpInt -= 2**buttonCounter
				elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
					gpInt -= 2**(buttonCounter + 1)
				else:
					pass
			buttonCounter += 2
	
	if relevantChanges > 0:
		return gpInt
	else:
		return None



# einzelne Prozesse
###################

def gpProcess(qGP):
	
	while 1:
		gpState = serializeGP()
		if(gpState != None):
			xmpp = SendMsgBot("ice@ice.midi.xyz", "me_like!")
			xmpp.sendData("ice@ice.midi.xyz", str(gpState))
			qGP.put(gpInt)
			print(gpState)



# das ganze Zeug ausführen
##########################

if __name__ == '__main__':
	qGP = Queue()
	pGP = Process(target=gpProcess, args=(qGP,))
	pGP.start()
	print("Gamepad Prozess gestartet")
	while 1:
		if not qGP.empty():
			gpInt = qGP.get()
			print("global: ", gpInt)
