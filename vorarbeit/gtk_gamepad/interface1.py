import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

class ButtonWindow(Gtk.Window):

	def __init__(self):
		Gtk.Window.__init__(self, title="Button Demo")
		self.set_border_width(10)

		hbox = Gtk.Box(spacing=6)
		self.add(hbox)

		anzClients = 2
		self.btnList = []

		for nBtn in range(0, anzClients):
			self.btnList.append(Gtk.Button("Client %d" % (nBtn+1)))
			self.btnList[nBtn].connect("clicked", self.printBtnNr, nBtn)
			self.btnList[nBtn].connect("clicked", self.markBtn, nBtn)
			hbox.pack_start(self.btnList[nBtn], True, True, 0)


	def printBtnNr(self, button, clientNr):
		print("called Client %d" % int(clientNr+1))


	def markBtn(self, button, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	def on_close_clicked(self, button):
		print("Closing application")
		Gtk.main_quit()


cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)


win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
