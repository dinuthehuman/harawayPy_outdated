from inputs import get_gamepad

buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
axesList = ["ABS_X", "ABS_Y"]
gpInt = 0

while 1:
	events = get_gamepad()
	buttonCounter = 0
	relevantChanges = 0
	
	for event in events:
		for eachButton in buttonList:
			if event.ev_type == "Key" and event.code == eachButton:
				relevantChanges = relevantChanges + 1
				if event.state == 1 and gpInt & 2**buttonCounter == 0:
					gpInt = gpInt + 2**buttonCounter
				elif gpInt & 2**buttonCounter > 0:
					gpInt = gpInt - 2**buttonCounter
			
			buttonCounter = buttonCounter + 1
			
		for eachAxe in axesList:
			if event.ev_type == "Absolute" and event.code == eachAxe:
				relevantChanges = relevantChanges + 1
				if event.state < 100 and gpInt & 2**buttonCounter == 0:
					gpInt = gpInt + 2**buttonCounter
				elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
					gpInt = gpInt + 2**(buttonCounter + 1)
				elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
					gpInt = gpInt - 2**buttonCounter
				elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
					gpInt = gpInt - 2**(buttonCounter + 1)
				else:
					pass
			
			buttonCounter = buttonCounter + 2
			
	if(relevantChanges > 0):
		print(gpInt)

