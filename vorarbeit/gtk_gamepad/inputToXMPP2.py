#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	SleekXMPP: The Sleek XMPP Library
	Copyright (C) 2010  Nathanael C. Fritz
	This file is part of SleekXMPP.

	See the file LICENSE for copying permission.
"""

import sys
import logging
import getpass
from optparse import OptionParser
from inputs import get_gamepad
import sleekxmpp


buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
axesList = ["ABS_X", "ABS_Y"]
gpInt = 0

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
	from sleekxmpp.util.misc_ops import setdefaultencoding
	setdefaultencoding('utf8')
else:
		raw_input = input


class StartMsgBot(sleekxmpp.ClientXMPP):

	"""
	A basic SleekXMPP bot that will log in, send a message,
	and then log out.
	"""

	def __init__(self, jid, password, recipient, message):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		print("Object created")
		# The message we wish to send, and the JID that
		# will receive it.
		self.recipient = recipient
		self.msg = message

		# The session_start event will be triggered when
		# the bot establishes its connection with the server
		# and the XML streams are ready for use. We want to
		# listen for this event so that we we can initialize
		# our roster.
		self.add_event_handler("session_start", self.start, threaded=True)

	def start(self, event):
		self.send_presence()
		self.get_roster()
		print("roster sent")
		#self.connect()
		#print("connected")
		
		
	def transmitData(self, data):
		self.msg = str(data)
		self.send_message(mto=self.recipient,
							mbody=self.msg,
							mtype='chat')
		
		self.process(block=True)
		# Using wait=True ensures that the send queue will be
		# emptied before ending the session.
		#self.disconnect(wait=True)


if __name__ == '__main__':
	
	xmpp = StartMsgBot("ice@ice.midi.xyz", "me_like!", "ice@ice.midi.xyz", "that went well")
	xmpp.register_plugin('xep_0030') # Service Discovery
	xmpp.register_plugin('xep_0199') # XMPP Ping
	
	while 1:
		events = get_gamepad()
		buttonCounter = 0
		relevantChanges = 0
		
		for event in events:
			for eachButton in buttonList:
				if event.ev_type == "Key" and event.code == eachButton:
					relevantChanges = relevantChanges + 1
					if event.state == 1 and gpInt & 2**buttonCounter == 0:
						gpInt = gpInt + 2**buttonCounter
					elif gpInt & 2**buttonCounter > 0:
						gpInt = gpInt - 2**buttonCounter
				
				buttonCounter = buttonCounter + 1
				
			for eachAxe in axesList:
				if event.ev_type == "Absolute" and event.code == eachAxe:
					relevantChanges = relevantChanges + 1
					if event.state < 100 and gpInt & 2**buttonCounter == 0:
						gpInt = gpInt + 2**buttonCounter
					elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
						gpInt = gpInt + 2**(buttonCounter + 1)
					elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
						gpInt = gpInt - 2**buttonCounter
					elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
						gpInt = gpInt - 2**(buttonCounter + 1)
					else:
						pass
				
				buttonCounter = buttonCounter + 2
				
		if(relevantChanges > 0):
			print(gpInt)
			xmpp.transmitData(gpInt)
			'''if xmpp.connect():
				xmpp.process(block=True)
				#xmpp.process()
				print("Done")
			else:
				print("Unable to connect.")'''
