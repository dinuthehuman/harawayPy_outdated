import sleekxmpp


class ReceiveMsgBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):

		#Übergeordnete Klasse initialisieren
		print("init Method started")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		#Instanz-Variablen festlegen
		self.room = room
		self.nick = nick

		#Event-Handler definieren
		self.add_event_handler("session_start", self.start, threaded=True)
		self.add_event_handler("groupchat_message", self.message)

	#Start-Methode: Verbindung etablieren
	def start(self, event):
		self.send_presence()
		self.get_roster()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick, wait=True)
		print("Start Method finished")

	#Callback für Empfang-Listener
	def message(self, msg):
		print("Event listener groupmessage fired")
		if msg['type'] in ('chat', 'normal', 'groupchat'):
			print("I just heard: %s" % msg['body'])

	#Methode, um Verbindung zu beenden
	def cutCon(self):
		self.disconnect(wait=True)



#Verpackungsklasse, um alles mit einer Zeile starten zu können
class ReceiveMsgWrapper(ReceiveMsgBot):

	def __init__(self, jid, password, room, nick):
		self.wrappedClient = ReceiveMsgBot(jid, password, room, nick)
		self.wrappedClient.register_plugin('xep_0045') # Multi-User Chat
		self.wrappedClient.register_plugin('xep_0030') # Service Discovery
		self.wrappedClient.register_plugin('xep_0199') # XMPP Ping
		self.wrappedClient.connect()
		self.wrappedClient.process()
		print("process executed")


	#Beenden-Methode
	def cutConnection(self):
		self.wrappedClient.cutCon()


#Das ganze Zeug ausführen
xmpp = ReceiveMsgWrapper("ice@ice.midi.xyz", "me_like!", "ctrl@conference.ice.midi.xyz", "ice")

while 1:
	pass
