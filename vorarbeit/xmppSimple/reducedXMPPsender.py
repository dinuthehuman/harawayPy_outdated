import sleekxmpp
from time import sleep


class SendMsgBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password):
		print("init Method started")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self.add_event_handler("session_start", self.start, threaded=True)

	def start(self, event):
		self.send_presence()
		self.get_roster()

	def sendData(self, gpRecipient, gpMsg):
		self.send_message(gpRecipient, gpMsg, 'chat')

	def cutCon(self):
		self.disconnect(wait=True)

class SendMsgWrapper(SendMsgBot):

	def __init__(self, jid, password):
		self.wrappedClient = SendMsgBot(jid, password)
		self.wrappedClient.connect()
		self.wrappedClient.process()

	def transmit(self, reciever, msg):
		self.wrappedClient.sendData(reciever, msg)

xmpp = SendMsgWrapper("ice@ice.midi.xyz", "me_like!")

while 1:
	xmpp.transmit("ice@ice.midi.xyz", "hi there!")
	sleep(2)
	print("Just slept")
