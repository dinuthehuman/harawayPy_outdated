import sleekxmpp

class EchoBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password):
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self.add_event_handler("session_start", self.start)
		self.add_event_handler("message", self.message)
		print("Event Listeners attached")

	def start(self, event):
		self.send_presence()
		self.get_roster()
		print("Start Method completed")


	def message(self, msg):
		print("Event Listener Message fired")
		if msg['type'] in ('chat', 'normal'):
			msg.reply("Thanks for sending\n%(body)s" % msg).send()
			self.send_message('admin@ice.midi.xyz', "hi there!")


if __name__ == '__main__':

	xmpp = EchoBot("ice@ice.midi.xyz", "me_like!")

	if xmpp.connect():
		print("XMPP connected")
		xmpp.process(block=True)
		print("Done")
	else:
		print("Unable to connect.")
