import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject
from inputs import get_gamepad
import threading
import sleekxmpp
import json


"""
Global Scope Variabeln
"""

#Zustand, in dem sich das Programm grad befindet
dispMenu = True
isConnected = False
dispOverlay = False
rolle = None
connectedTo = None

#Gamepad Transmitter; globalisiert, weil von sehr unterschiedlichen Orten drauf zugegriffen werden muss; nicht ganz sicher, ob das nötig ist.
gpInt = 0
gpXMPP = None

"""
Uitlity/Helfer-Funktionen
"""

#JSON validieren/Error Handling
def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError:
		return False
	return True


"""
ctrl XMPP Event Emitter
hört auf den ctrl Chatroom,
emittiert einen Event, wenn die receiver-Komponente der eigenen rolle entspricht
aufgeteilt auf Klasse, die verbindet und Verpackung, um connect()-Kollision zu vermeiden
"""
class xmppBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):

		#Übergeordnete Klasse initialisieren
		print("init Method started")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		#Instanz-Variablen festlegen
		self.room = room
		self.nick = nick

		#Event-Handler definieren
		self.add_event_handler("session_start", self.start, threaded=False)
		#self.add_event_handler("groupchat_message", self.message) #NOPE! Erst die Verpackungsklasse hängt diesen Event Handler an

	#Start-Methode: Verbindung etablieren
	def start(self, event):
		self.send_presence()
		self.get_roster()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick, wait=True)
		print("Start Method finished")


#Verpackungsklasse, um alles mit einer Zeile starten zu können & connect()-Kollision umgehen
#Erst diese Klasse hängt einen Event Handler an xmppBot, die selbst keinen für die Nachrichten hat; die Verpackungsklasse muss auf den Event reagieren, sonst connect()-Kollision
class xmppWrapper(GObject.GObject):

	__gsignals__ = {
		'relevantMsg': (GObject.SIGNAL_RUN_FIRST, None, (str,str,str,)),
	}

	def __init__(self, jid, password, room, nick):
		GObject.GObject.__init__(self)
		self.ready = False
		self.room = room
		self.wrappedClient = xmppBot(jid, password, room, nick)
		self.wrappedClient.add_event_handler("groupchat_message", self.message) #Hier wird der Event Handler an die xmppBot-Instanz gehängt
		self.wrappedClient.register_plugin('xep_0045') # Multi-User Chat
		self.wrappedClient.register_plugin('xep_0030') # Service Discovery
		self.wrappedClient.register_plugin('xep_0199') # XMPP Ping
		self.wrappedClient.connect()
		self.wrappedClient.process()
		self.ready = True
		print("process executed")

	def message(self, msg):
		print("Event listener groupmessage fired")
		if is_json(msg["body"]):
			decodedMsg = json.loads(msg["body"])
			if "receiver" in decodedMsg:
				if decodedMsg["receiver"] == rolle:
					if "sender" in decodedMsg:
						emitSender = decodedMsg["sender"]
					else:
						emitSender = None
					if "action" in decodedMsg:
						emitAction = decodedMsg["action"]
					else:
						emitAction = None
					if "value" in decodedMsg:
						emitValue = decodedMsg["value"]
					else:
						emitValue = None
					self.emit("relevantMsg", emitSender, emitAction, emitValue)
				else:
					pass

	def send(self, msg):
		self.wrappedClient.send_message(mto=self.room, mbody=msg, mtype="groupchat")


	#Beenden-Methode
	def cutConnection(self):
		self.wrappedClient.disconnect()



"""
Gamepad Event Emitter
"""
class gpEventEmitter(GObject.GObject):

	__gsignals__ = {
		'relevantButton': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
	}


	def __init__(self):
		GObject.GObject.__init__(self)


	def startListening(self):
		while 1:
			events = get_gamepad()
			for event in events:

				# Event emitter für Client Menu Führung
				if dispMenu:
					if event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start") #Signal, das abgefeuert wird + die Argumente, die es mitliefern muss
					elif event.ev_type == "Absolute" and event.code == "ABS_Y":
						if event.state < 100:
							self.emit("relevantButton", "up")
						elif event.state > 200:
							self.emit("relevantButton", "down")

				# Event emitter für Client Interface
				elif not dispMenu:

					#Gamepad-Zustand an Cyborg senden - falls nötig
					if isConnected and not dispOverlay and gpXMPP is not None:

						if gpXMPP.ready:
							global gpInt
							buttonList = ["BTN_TRIGGER", "BTN_THUMB","BTN_THUMB2","BTN_TOP"]
							axesList = ["ABS_X", "ABS_Y"]
							buttonCounter = 0
							relevantChanges = 0

							#Alle Buttons durchschleifen
							for eachButton in buttonList:
								if event.ev_type == "Key" and event.code == eachButton:
									relevantChanges += 1
									if event.state == 1 and gpInt & 2**buttonCounter == 0:
										gpInt += 2**buttonCounter
									elif gpInt & 2**buttonCounter > 0:
										gpInt -= 2**buttonCounter
									else:
										pass
								buttonCounter += 1

							#Alle Achsen durchschleifen
							for eachAxe in axesList:
								if event.ev_type == "Absolute" and event.code == eachAxe:
									relevantChanges += 1
									if event.state < 100 and gpInt & 2**buttonCounter == 0:
										gpInt += 2**buttonCounter
									elif event.state > 200 and gpInt & 2**(buttonCounter + 1) == 0:
										gpInt += 2**(buttonCounter + 1)
									elif gpInt & (2**buttonCounter) > 0 and gpInt & 2**buttonCounter > 0:
										gpInt -= 2**buttonCounter
									elif gpInt & (2**(buttonCounter + 1)) > 0 and gpInt & 2**(buttonCounter + 1) > 0:
										gpInt -= 2**(buttonCounter + 1)
									else:
										pass
								buttonCounter += 2

							#Nachricht bauen und senden
							if relevantChanges > 0:
								gpMsg = {}
								gpMsg["sender"] = rolle
								gpMsg["receiver"] = connectedTo
								gpMsg["action"] = "gamepad"
								gpMsg["value"] = gpInt
								gpXMPP.send(json.dumps(gpMsg))


					# Overlay triggern
					if not dispOverlay and event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start")
					# Zurück zum Menu
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_THUMB" and event.state == 1:
						self.emit("relevantButton", "rot")
					# Overlay wieder verstecken
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_TOP" and event.state == 1:
						self.emit("relevantButton", "grun")



"""
Die GTK-Klasse, die das effektive Fenster zeichnet und an der alle Event Listeners hängen
"""
class ButtonWindow(Gtk.Window):

	def __init__(self):

		"""
		Zuerst die graphischen Dinge, die Fenster und Bedienelemente werden gezeichnet
		"""

		#Konfiguration der Klasse
		self.anzClients = 2 #Wieviele Clients gibt's, wieviele Buttons werden gezeichnet

		#übergeordnete Klasse (GTK) wird initialisiert
		Gtk.Window.__init__(self, title="Button Demo")

		#GTK global
		self.set_border_width(10)
		self.menubox = Gtk.Box(spacing=6)
		self.add(self.menubox)

		#GTK für die Client-Menuführung
		self.btnList = []  #Hierrein werden die Button-Objekte gespeichert
		self.selectedButton = -1 #Speicher, welcher Button im Moment durch das Gamepad selektioniert wurde
		for nBtn in range(0, self.anzClients): #Für jeden Client einen Button zeichnen und in btnList speichern
			self.btnList.append(Gtk.Button("Client %d" % (nBtn + 1))) #Text draufschreiben
			self.btnList[nBtn].connect("clicked", self.dispIF, "client%d" % nBtn) #Event Listener/auszuführende Funktion anhängen
			self.menubox.pack_start(self.btnList[nBtn], True, True, 0) #Keine Ahnung, was das macht

		#GTK für das Client-Interface
		self.ifTestLabel = Gtk.Label("Hier VLC imaginieren")
		self.ifOverlay = Gtk.Label("Wirklich beenden? rot: JA! // grün: NEIN!")
		self.menubox.pack_start(self.ifTestLabel, True, True, 0)
		self.menubox.pack_start(self.ifOverlay, True, True, 0)

		"""
		Alle Event Listeners werden definiert und an die GTK-Instanz gehängt
		Wo nötig werden eigene Threads für die Event Emitter gestartet
		"""

		#XMPP-Listener für ctrl-Room, an GTK hängen
		self.ctrlXMPP = xmppWrapper("ice@ice.midi.xyz", "me_like!", "ctrl@conference.ice.midi.xyz", "ice")
		self.ctrlXMPP.connect("relevantMsg", self.ctrlDetected)

		#Gamepad-Listener an GTK anhängen
		gpListener = gpEventEmitter()
		gpListener.connect("relevantButton", self.buttonDetected) #Event Listener anhängen: Signal-Name, Callback -> die Argumente, die dem Callback übergeben werden, sind definiert durch die emit-Funktion
		gpThread = threading.Thread(target=gpListener.startListening, args=()) #Aus der Methode, die den Loop enthält, wird ein Thread gemacht
		gpThread.start() #Der Thread/Loop wird gestartet


	"""
	ctrl XMPP Callback
	"""
	def ctrlDetected(self, data1, sender, action, value):
		print("Message detected")
		print("I'll have to %s" %action)

		#Verbindung zu einem Cyborg herstellen
		if action == "connect":
			self.connectCyborg(sender)

		#Cyborg will die Verbindung beenden
		if action == "disconnect":
			self.disconnectCyborg(setResponse=False)


	"""
	Gamepad Listener Callback, an GTK angehänt
	"""
	def buttonDetected(self, data1, data2):
		print("button detected!")
		print(data2)

		if dispMenu:
			if data2 == "down":
				if self.selectedButton < self.anzClients - 1:
					self.selectedButton += 1
				self.markBtn(self.selectedButton)
			elif data2 == "up":
				if self.selectedButton > 0:
					self.selectedButton -= 1
				self.markBtn(self.selectedButton)
			elif data2 == "start" and self.selectedButton >= 0:
				self.dispIF(None, "client%d" % self.selectedButton) #erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist

		elif not dispMenu:
			if data2 == "start" and not dispOverlay:
				self.dispOverlay(True)
			elif data2 == "grun" and dispOverlay:
				self.dispOverlay(False)
			elif data2 == "rot" and dispOverlay:
				self.dispMenu()


	"""
	Verbindung zu Cyborgs herstellen oder abbrechen
	"""

	def connectCyborg(self, cyborg):
		global gpXMPP, isConnected, connectedTo
		xmppResponse = {}
		#Es steht schon eine Verbindung, keine neue herstellen
		if isConnected:
			xmppResponse["action"] = "stillConnected"
			xmppResponse["value"] = connectedTo
		#Es steht noch keine Verbindung, herstellen!
		else:
			#Neuer Room für Gamepad-Chat
			gpXMPP =  xmppWrapper("ice@ice.midi.xyz", "me_like!", "%s@conference.ice.midi.xyz" % rolle, "ice")
			#Auf XMPP-Verbindung warten
			while not gpXMPP.ready:
				pass
			#globale Variabeln anpassen
			isConnected = True
			connectedTo = cyborg
			#TODO: VLC-Streams hier aufbauen
			#TODO: irgendeine GTK-Feedback-Message einbauen
			#Ready-Mitteilung an Cyborg
			xmppResponse["action"] = "ready"
		#Rückmeldung abschicken
		xmppResponse["sender"] = rolle
		xmppResponse["receiver"] = cyborg
		self.ctrlXMPP.send(json.dumps(xmppResponse))


	def disconnectCyborg(self, setResponse):
		global gpXMPP, isConnected, connectedTo
		if setResponse:
			xmppResponse = {}
			xmppResponse["sender"] = rolle
			xmppResponse["receiver"] = connectedTo
			xmppResponse["action"] = "disconnect"
			self.ctrlXMPP.send(json.dumps(xmppResponse))
		if gpXMPP is not None:
			gpXMPP.cutConnection()
			gpXMPP = None
		#TODO: VLC-Streams hier abbrechen
		#TODO: irgendein Feedback
		isConnected = False
		connectedTo = None


	"""
	Grafische Funktionen, Steuerung der Menuführung
	"""
	#Das Markieren der Buttons. CSS-Klassen entfernen und anhängen, nur optisch für Menuführung relevant
	def markBtn(self, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	#Client Interface ausblenden, Menu anzeigen, Verbindung abbrechen
	def dispMenu(self):
		global rolle, dispMenu, dispOverlay
		self.disconnectCyborg(setResponse=True)
		#globale Variabeln umdefinieren
		rolle = None
		dispMenu = True
		dispOverlay = False
		self.ifTestLabel.hide()
		self.ifOverlay.hide()
		for btn in self.btnList:
			btn.show()


	#Menu ausblenden, Client Interface anzeigen
	def dispIF(self, button, setRolle):
		global rolle, dispMenu
		rolle = setRolle
		print("Rolle gewählt: %s" % rolle)
		dispMenu = False
		for btn in self.btnList:
			btn.hide()
		self.ifTestLabel.show()


	#Quit Overlay anzeigen
	def dispOverlay(self, setOverlay):
		global dispOverlay
		if setOverlay:
			dispOverlay = True
			self.ifOverlay.show()
		else:
			dispOverlay = False
			self.ifOverlay.hide()


	def on_close_clicked(self, button):
		self.disconnectCyborg(setResponse=True)
		print("Closing application")
		Gtk.main_quit()



"""
CSS-File integrieren
"""
cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)


"""
GTK-Applikation starten
"""
win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
win.dispMenu()
Gtk.main()
