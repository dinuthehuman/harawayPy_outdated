
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject
from inputs import get_gamepad
import threading
import sleekxmpp
import json


"""
Global Scope Variabeln
Zustand, in dem sich das Programm grad befindet
"""
dispMenu = True
isConnected = False
dispOverlay = False
rolle = None
connectedTo = None


"""
globalisierte XMPP-Objekte
"""
ctrlListener = None #Muss globalsiert werden wegen connect-Namenskollision

"""
Uitlity/Helfer-Funktionen
"""

def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError:
		return False
	return True


"""
ctrl XMPP Event Emitter
hört auf den ctrl Chatroom,
emittiert einen Event, wenn die receiver-Komponente der eigenen rolle entspricht
"""
class ReceiveMsgBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):

		#Übergeordnete Klasse initialisieren
		print("init Method started")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		#Instanz-Variablen festlegen
		self.room = room
		self.nick = nick

		#Event-Handler definieren
		self.add_event_handler("session_start", self.start, threaded=True)
		self.add_event_handler("groupchat_message", self.message)

	#Start-Methode: Verbindung etablieren
	def start(self, event):
		self.send_presence()
		self.get_roster()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick, wait=True)
		print("Start Method finished")

	#Callback für Empfang-Listener
	def message(self, msg):
		print("Event listener groupmessage fired for")
		if is_json(msg["body"]):
			decodedMsg = json.loads(msg["body"])
			if "receiver" in decodedMsg:
				if decodedMsg["receiver"] == rolle:
					if "sender" in decodedMsg:
						emitSender = decodedMsg["sender"]
					else:
						emitSender = None
					if "action" in decodedMsg:
						emitAction = decodedMsg["action"]
					else:
						emitAction = None
					if "value" in decodedMsg:
						emitValue = decodedMsg["value"]
					else:
						emitValue = None
					ctrlListener.emitter(emitSender, emitAction, emitValue) # absolut benannt, greift auf global scope zu wegen connect()-Namenskollision

	#Methode, um Verbindung zu beenden
	def cutCon(self):
		self.disconnect(wait=True)



#Verpackungsklasse, um alles mit einer Zeile starten zu können
class ReceiveMsgWrapper(GObject.GObject):

	__gsignals__ = {
		'relevantMsg': (GObject.SIGNAL_RUN_FIRST, None, (str,str,str,)),
	}

	def __init__(self, jid, password, room, nick):
		GObject.GObject.__init__(self)
		self.wrappedClient = ReceiveMsgBot(jid, password, room, nick)
		self.wrappedClient.register_plugin('xep_0045') # Multi-User Chat
		self.wrappedClient.register_plugin('xep_0030') # Service Discovery
		self.wrappedClient.register_plugin('xep_0199') # XMPP Ping
		self.wrappedClient.connect()
		self.wrappedClient.process()
		print("process executed")

	def emitter(self, sender, action, value):
		self.emit("relevantMsg", sender, action, value)

	#Beenden-Methode
	def cutConnection(self):
		self.wrappedClient.cutCon()



"""
Gamepad Event Emitter
"""
class gpEventEmitter(GObject.GObject):

	__gsignals__ = {
		'relevantButton': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
	}


	def __init__(self):
		GObject.GObject.__init__(self)


	def startListening(self):
		while 1:
			events = get_gamepad()
			for event in events:

				# Event emitter für Client Menu Führung
				if dispMenu:
					if event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start") #Signal, das abgefeuert wird + die Argumente, die es mitliefern muss
					elif event.ev_type == "Absolute" and event.code == "ABS_Y":
						if event.state < 100:
							self.emit("relevantButton", "up")
						elif event.state > 200:
							self.emit("relevantButton", "down")

				# Event emitter für Client Interface
				elif not dispMenu:

					if isConnected:
						pass #HIER DANN GAMEPAD XMPP EINBAUEN!

					# Overlay triggern
					if not dispOverlay and event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
						self.emit("relevantButton", "start")
					# Zurück zum Menu
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_THUMB" and event.state == 1:
						self.emit("relevantButton", "rot")
					# Overlay wieder verstecken
					elif dispOverlay and event.ev_type == "Key" and event.code == "BTN_TOP" and event.state == 1:
						self.emit("relevantButton", "grun")



"""
Die GTK-Klasse, die das effektive Fenster zeichnet und an der alle Event Listeners hängen
"""
class ButtonWindow(Gtk.Window):

	def __init__(self):

		"""
		Zuerst die graphischen Dinge, die Fenster und Bedienelemente werden gezeichnet
		"""

		#Konfiguration der Klasse
		self.anzClients = 2 #Wieviele Clients gibt's, wieviele Buttons werden gezeichnet

		#übergeordnete Klasse (GTK) wird initialisiert
		Gtk.Window.__init__(self, title="Button Demo")

		#GTK global
		self.set_border_width(10)
		self.menubox = Gtk.Box(spacing=6)
		self.add(self.menubox)

		#GTK für die Client-Menuführung
		self.btnList = []  #Hierrein werden die Button-Objekte gespeichert
		self.selectedButton = -1 #Speicher, welcher Button im Moment durch das Gamepad selektioniert wurde
		for nBtn in range(0, self.anzClients): #Für jeden Client einen Button zeichnen und in btnList speichern
			self.btnList.append(Gtk.Button("Client %d" % (nBtn + 1))) #Text draufschreiben
			self.btnList[nBtn].connect("clicked", self.dispIF, "client%d" % nBtn) #Event Listener/auszuführende Funktion anhängen
			self.menubox.pack_start(self.btnList[nBtn], True, True, 0) #Keine Ahnung, was das macht

		#GTK für das Client-Interface
		self.ifTestLabel = Gtk.Label("Hier VLC imaginieren")
		self.ifOverlay = Gtk.Label("Wirklich beenden? rot: JA! // grün: NEIN!")
		self.menubox.pack_start(self.ifTestLabel, True, True, 0)
		self.menubox.pack_start(self.ifOverlay, True, True, 0)

		"""
		Alle Event Listeners werden definiert und an die GTK-Klasse gehängt
		Wo nötig werden eigene Threads für die Event Emitter gestartet
		"""

		#XMPP-Listener für ctrl-Room, an GTK hängen
		global ctrlListener #globalisiert wegen connect()-Namenskollision
		ctrlListener = ReceiveMsgWrapper("ice@ice.midi.xyz", "me_like!", "ctrl@conference.ice.midi.xyz", "ice")
		ctrlListener.connect("relevantMsg", self.msgDetected)

		#Gamepad-Listener an GTK anhängen
		gpListener = gpEventEmitter()
		gpListener.connect("relevantButton", self.buttonDetected) #Event Listener anhängen: Signal-Name, Callback -> die Argumente, die dem Callback übergeben werden, sind definiert durch die emit-Funktion
		gpThread = threading.Thread(target=gpListener.startListening, args=()) #Aus der Methode, die den Loop enthält, wird ein Thread gemacht
		gpThread.start() #Der Thread/Loop wird gestartet


	"""
	ctrl XMPP Callback
	"""
	def msgDetected(self, data1, detSender, detAction, detValue):
		print("Message detected")
		print("I'll have to %s" %detAction)


	"""
	Gamepad Listener Callback, an GTK angehänt
	"""
	def buttonDetected(self, data1, data2):
		print("button detected!")
		print(data2)

		if dispMenu:
			if data2 == "down":
				if self.selectedButton < self.anzClients - 1:
					self.selectedButton += 1
				self.markBtn(self.selectedButton)
			elif data2 == "up":
				if self.selectedButton > 0:
					self.selectedButton -= 1
				self.markBtn(self.selectedButton)
			elif data2 == "start" and self.selectedButton >= 0:
				self.dispIF(None, "client%d" % self.selectedButton) #erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist

		elif not dispMenu:
			if data2 == "start" and not dispOverlay:
				self.dispOverlay(True)
			elif data2 == "grun" and dispOverlay:
				self.dispOverlay(False)
			elif data2 == "rot" and dispOverlay:
				self.dispMenu()


	"""
	Grafische Funktionen, Steuerung der Menuführung
	"""
	#Das Markieren der Buttons. CSS-Klassen entfernen und anhängen, nur optisch für Menuführung relevant
	def markBtn(self, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	#Client Interface ausblenden, Menu anzeigen
	def dispMenu(self):
		global rolle, dispMenu, dispOverlay, isConnected, connectedTo
		rolle = None
		dispMenu = True
		dispOverlay = False
		isConnected = False
		connectedTo = False
		self.ifTestLabel.hide()
		self.ifOverlay.hide()
		for btn in self.btnList:
			btn.show()


	#Menu ausblenden, Client Interface anzeigen
	def dispIF(self, button, setRolle):
		global rolle, dispMenu
		rolle = setRolle
		print("Rolle gewählt: %s" % rolle)
		dispMenu = False
		for btn in self.btnList:
			btn.hide()
		self.ifTestLabel.show()


	#Quit Overlay anzeigen
	def dispOverlay(self, setOverlay):
		global dispOverlay
		if setOverlay:
			dispOverlay = True
			self.ifOverlay.show()
		else:
			dispOverlay = False
			self.ifOverlay.hide()


	def on_close_clicked(self, button):
		print("Closing application")
		Gtk.main_quit()



"""
CSS-File integrieren
"""
cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)


"""
GTK-Applikation starten
"""
win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
win.dispMenu()
Gtk.main()
