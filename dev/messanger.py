from xmppInformation import XMPPInformation
import json

class Messanger:
    def __init__(self):
        self.gpmessage = {}

    def buildMessage(self, sender, receiver, action, value):
        self.gpmessage["sender"] = sender
        self.gpmessage["receiver"] = receiver
        self.gpmessage["action"] = action
        self.gpmessage["value"] = value

    def sendMessage(self, GpXmpp):
        GpXmpp.send(json.dumps(self.gpmessage))