import sleekxmpp
import json
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject

isConnected = False
rolle = None
connectedTo = None

"""
Uitlity/Helfer-Funktionen
"""

#JSON validieren/Error Handling
def is_json(myjson):
	try:
		json_object = json.loads(myjson)
	except ValueError:
		return False
	return True


"""
ctrl XMPP Event Emitter
hört auf den ctrl Chatroom,
emittiert einen Event, wenn die receiver-Komponente der eigenen rolle entspricht
aufgeteilt auf Klasse, die verbindet und Verpackung, um connect()-Kollision zu vermeiden
"""
class xmppBot(sleekxmpp.ClientXMPP):

	def __init__(self, jid, password, room, nick):

		#Übergeordnete Klasse initialisieren
		print("init Method started")
		sleekxmpp.ClientXMPP.__init__(self, jid, password)

		#Instanz-Variablen festlegen
		self.room = room
		self.nick = nick

		#Event-Handler definieren
		self.add_event_handler("session_start", self.start, threaded=False)
		#self.add_event_handler("groupchat_message", self.message) #NOPE! Erst die Verpackungsklasse hängt diesen Event Handler an

	#Start-Methode: Verbindung etablieren
	def start(self, event):
		self.send_presence()
		self.get_roster()
		self.plugin['xep_0045'].joinMUC(self.room, self.nick, wait=True)
		print("Start Method finished")


#Verpackungsklasse, um alles mit einer Zeile starten zu können & connect()-Kollision umgehen
#Erst diese Klasse hängt einen Event Handler an xmppBot, die selbst keinen für die Nachrichten hat; die Verpackungsklasse muss auf den Event reagieren, sonst connect()-Kollision
class xmppWrapper(GObject.GObject):

	__gsignals__ = {
		'relevantMsg': (GObject.SIGNAL_RUN_FIRST, None, (str,str,str,)),
	}

	def __init__(self, jid, password, room, nick):
		GObject.GObject.__init__(self)
		self.ready = False
		self.room = room
		self.wrappedClient = xmppBot(jid, password, room, nick)
		self.wrappedClient.add_event_handler("groupchat_message", self.message) #Hier wird der Event Handler an die xmppBot-Instanz gehängt
		self.wrappedClient.register_plugin('xep_0045') # Multi-User Chat
		self.wrappedClient.register_plugin('xep_0030') # Service Discovery
		self.wrappedClient.register_plugin('xep_0199') # XMPP Ping
		self.wrappedClient.connect()
		self.wrappedClient.process()
		self.ready = True
		print("process executed")

	def message(self, msg):
		print("Event listener groupmessage fired")
		if is_json(msg["body"]):
			decodedMsg = json.loads(msg["body"])
			if "receiver" in decodedMsg:
				if decodedMsg["receiver"] == rolle:
					if "sender" in decodedMsg:
						emitSender = decodedMsg["sender"]
					else:
						emitSender = None
					if "action" in decodedMsg:
						emitAction = decodedMsg["action"]
					else:
						emitAction = None
					if "value" in decodedMsg:
						emitValue = decodedMsg["value"]
					else:
						emitValue = None
					self.emit("relevantMsg", emitSender, emitAction, emitValue)
				else:
					pass

	def send(self, msg):
		self.wrappedClient.send_message(mto=self.room, mbody=msg, mtype="groupchat")


	#Beenden-Methode
	def cutConnection(self):
		self.wrappedClient.disconnect()


