import configparser

import gi
from inputs import get_gamepad

import harawayShared

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject
from menuState import MenuState
from overlayState import OverlayState
from gpCalculator import GpCalculator
from xmppInformation import XMPPInformation
from messanger import Messanger
import threading
import json


"""
Global Scope Variabeln
"""

#Config-File laden
config = configparser.ConfigParser()
config.read('config.ini')

#States
Menu = MenuState()
Overlay = OverlayState()

#Global Classes
GPXMPP = XMPPInformation()
GPCalculator = GpCalculator()
GpMessanger = Messanger()

"""
Gamepad Event Emitter
"""
class gpEventEmitter(GObject.GObject):

	__gsignals__ = {
		'relevantButton': (GObject.SIGNAL_RUN_FIRST, None, (str,)),
	}


	def __init__(self):
		GObject.GObject.__init__(self)


	def startListening(self):
		while 1:
			events = get_gamepad()
			for event in events:

				# Event emitter für Client Menu Führung
				if Menu.getDispMenu():
					self.clientInMenu(event)
				# Event emitter für Client Interface
				elif not Menu.getDispMenu():
					self.clientInGame(event)


	"""
	Game
	"""
	def clientInGame(self, event):
		# Gamepad-Zustand an Cyborg senden - falls nötig
		activeOverlay = Overlay.getDispOverlay();
		if harawayShared.isConnected and not activeOverlay and GPXMPP.getGpXMPP() is not None:

			if GPXMPP.getGpXMPP().ready:
				GpCalculator.checkButtonClicked(event)
				GpCalculator.CheckAxisClicked(event)
				self.sendMessage()
		self.overlay(event)

	"""
	Menu
	"""
	def clientInMenu(self, event):
		if event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
			self.startButton()
		elif event.ev_type == "Absolute" and event.code == "ABS_Y":
			if event.state < 100:
				self.upButton()
			elif event.state > 200:
				self.downButton()

	"""
	Message
	"""
	def sendMessage(self):
		# Nachricht bauen und senden
		if GpCalculator.getRelevantChanges() > 0:
			GpMessanger.buildMessage(harawayShared.rolle, harawayShared.connectedTo, "gamepad", GpCalculator.getGpInt())
			GpMessanger.sendMessage(GPXMPP.getGpXMPP())

	"""
	Overlay
	"""
	def overlay(self,event):
		#Show Overlay
		if not Overlay.getDispOverlay() and event.ev_type == "Key" and event.code == "BTN_BASE4" and event.state == 1:
			self.startButton()
		#Back to Menu
		elif Overlay.getDispOverlay() and event.ev_type == "Key" and event.code == "BTN_THUMB" and event.state == 1:
			self.redButton()
		#Hide Overlay
		elif Overlay.getDispOverlay() and event.ev_type == "Key" and event.code == "BTN_TOP" and event.state == 1:
			self.greenButton()

	"""
	Button
	"""
	def greenButton(self):
		self.emit("relevantButton", "grun")

	def redButton(self):
		self.emit("relevantButton", "rot")

	def startButton(self):
		self.emit("relevantButton", "start")

	def downButton(self):
		self.emit("relevantButton", "down")

	def upButton(self):
		self.emit("relevantButton", "up")







"""
Die GTK-Klasse, die das effektive Fenster zeichnet und an der alle Event Listeners hängen
"""
class ButtonWindow(Gtk.Window):

	def __init__(self):

		"""
		Zuerst die graphischen Dinge, die Fenster und Bedienelemente werden gezeichnet
		"""

		#Konfiguration der Klasse
		self.anzClients = int(config['DEFAULT']['anzClients']) #Wieviele Clients gibt's, wieviele Buttons werden gezeichnet

		#übergeordnete Klasse (GTK) wird initialisiert
		Gtk.Window.__init__(self, title="Button Demo")


		#GTK global
		self.set_border_width(10)
		self.menubox = Gtk.Box(spacing=6)
		self.add(self.menubox)

		#GTK für die Client-Menuführung
		self.btnList = []  #Hierrein werden die Button-Objekte gespeichert
		self.selectedButton = -1 #Speicher, welcher Button im Moment durch das Gamepad selektioniert wurde
		for nBtn in range(0, self.anzClients): #Für jeden Client einen Button zeichnen und in btnList speichern
			self.btnList.append(Gtk.Button("Client %d" % (nBtn + 1))) #Text draufschreiben
			self.btnList[nBtn].connect("clicked", self.dispIF, "client%d" % nBtn) #Event Listener/auszuführende Funktion anhängen
			self.menubox.pack_start(self.btnList[nBtn], True, True, 0) #Keine Ahnung, was das macht

		#GTK für das Client-Interface
		self.ifTestLabel = Gtk.Label("Hier VLC imaginieren")
		self.ifOverlay = Gtk.Label("Wirklich beenden? rot: JA! // grün: NEIN!")
		self.menubox.pack_start(self.ifTestLabel, True, True, 0)
		self.menubox.pack_start(self.ifOverlay, True, True, 0)

		"""
		Alle Event Listeners werden definiert und an die GTK-Instanz gehängt
		Wo nötig werden eigene Threads für die Event Emitter gestartet
		"""

		#XMPP-Listener für ctrl-Room, an GTK hängen
		self.ctrlXMPP = harawayShared.xmppWrapper("%s@%s" % (config['xmpp']['user'], config['DEFAULT']['serverName']), config['xmpp']['password'], "%s@%s.%s" % (config['xmpp']['mainRoom'], config['xmpp']['service'], config['DEFAULT']['serverName']), config['xmpp']['user'])
		self.ctrlXMPP.connect("relevantMsg", self.ctrlDetected)

		#Gamepad-Listener an GTK anhängen
		gpListener = gpEventEmitter()
		gpListener.connect("relevantButton", self.buttonDetected) #Event Listener anhängen: Signal-Name, Callback -> die Argumente, die dem Callback übergeben werden, sind definiert durch die emit-Funktion
		gpThread = threading.Thread(target=gpListener.startListening, args=()) #Aus der Methode, die den Loop enthält, wird ein Thread gemacht
		gpThread.start() #Der Thread/Loop wird gestartet


	"""
	ctrl XMPP Callback
	"""
	def ctrlDetected(self, data1, sender, action, value):
		print("Message detected")
		print("I'll have to %s" %action)

		#Verbindung zu einem Cyborg herstellen
		if action == "connect":
			self.connectCyborg(sender)

		#Cyborg will die Verbindung beenden
		if action == "disconnect":
			self.disconnectCyborg(setResponse=False)


	"""
	Gamepad Listener Callback, an GTK angehänt
	"""
	def buttonDetected(self, data1, data2):
		print("button detected!")
		print(data2)

		if Menu.getDispMenu():
			self.MenuEnabled(data2)
		elif not Menu.getDispMenu():
			self.MenuDisabled(data2)


	def MenuEnabled(self, data2):
		if data2 == "down":
			if self.selectedButton < self.anzClients - 1:
				self.selectedButton += 1
			self.markBtn(self.selectedButton)
		elif data2 == "up":
			if self.selectedButton > 0:
				self.selectedButton -= 1
			self.markBtn(self.selectedButton)
		elif data2 == "start" and self.selectedButton >= 0:
			self.dispIF(None,"client%d" % self.selectedButton)  # erstes Argument None, weil beim Gamepad kein GTK-Button gedrückt wurde, dieser jedoch aufgrund des Button-Eventlisteners ein Parameter ist

	def MenuDisabled(self, data2):
		if data2 == "start" and not Overlay.getDispOverlay():
			self.displayOverlay(True)
		elif data2 == "grun" and Overlay.getDispOverlay():
			self.displayOverlay(False)
		elif data2 == "rot" and Overlay.getDispOverlay():
			self.displayMenu()


	"""
	Verbindung zu Cyborgs herstellen oder abbrechen
	"""

	def connectCyborg(self, cyborg):
		xmppResponse = {}
		#Es steht schon eine Verbindung, keine neue herstellen
		if harawayShared.isConnected:
			self.isConnectedCyborg(xmppResponse)
		#Es steht noch keine Verbindung, herstellen!
		else:
			#Neuer Room für Gamepad-Chat
			GPXMPP.setGpXMPP(harawayShared.xmppWrapper("%s@%s" % (config['xmpp']['user'], config['DEFAULT']['serverName']), config['xmpp']['password'], "%s@%s.%s" % (harawayShared.rolle, config['xmpp']['service'], config['DEFAULT']['serverName']), config['xmpp']['user']))
			#Auf XMPP-Verbindung warten
			while not XMPPInformation.getGpXMPP().ready:
				pass
			#globale Variabeln anpassen
			harawayShared.isConnected = True
			harawayShared.connectedTo = cyborg
			#TODO: VLC-Streams hier aufbauen
			#TODO: irgendeine GTK-Feedback-Message einbauen
			#Ready-Mitteilung an Cyborg
			xmppResponse["action"] = "ready"
		#Rückmeldung abschicken
		xmppResponse["sender"] = harawayShared.rolle
		xmppResponse["receiver"] = cyborg
		self.ctrlXMPP.send(json.dumps(xmppResponse))

	def isConnectedCyborg(self, xmppResponse):
		xmppResponse["action"] = "stillConnected"
		xmppResponse["value"] = harawayShared.connectedTo

	def disconnectCyborg(self, setResponse):
		if setResponse:
			xmppResponse = {}
			xmppResponse["sender"] = harawayShared.rolle
			xmppResponse["receiver"] = harawayShared.connectedTo
			xmppResponse["action"] = "disconnect"
			self.ctrlXMPP.send(json.dumps(xmppResponse))

		if GPXMPP.getGpXMPP() is not None:
			GPXMPP.getGpXMPP().cutConnection()
			GPXMPP.setGpXMPP(None)
		#TODO: VLC-Streams hier abbrechen
		#TODO: irgendein Feedback
		harawayShared.isConnected = False
		harawayShared.connectedTo = None


	"""
	Grafische Funktionen, Steuerung der Menuführung
	"""
	#Das Markieren der Buttons. CSS-Klassen entfernen und anhängen, nur optisch für Menuführung relevant
	def markBtn(self, whichBtn):
		for btn in self.btnList:
			ctx = btn.get_style_context()
			ctx.remove_class("marked")
		ctx = self.btnList[whichBtn].get_style_context()
		ctx.add_class("marked")


	#Client Interface ausblenden, Menu anzeigen, Verbindung abbrechen
	def displayMenu(self):
		self.disconnectCyborg(setResponse=True)
		#globale Variabeln umdefinieren
		harawayShared.rolle = None
		Menu.setDispMenu(True)
		Overlay.setDispOverlay(False)
		self.ifTestLabel.hide()
		self.ifOverlay.hide()
		for btn in self.btnList:
			btn.show()


	#Menu ausblenden, Client Interface anzeigen
	def dispIF(self, button, setRolle):
		harawayShared.rolle = setRolle
		print("Rolle gewählt: %s" % harawayShared.rolle)
		Menu.setDispMenu(False)
		for btn in self.btnList:
			btn.hide()
		self.ifTestLabel.show()


	#Quit Overlay anzeigen
	def displayOverlay(self, setOverlay):

		if setOverlay:
			Overlay.setDispOverlay(True)
			self.ifOverlay.show()
		else:
			Overlay.setDispOverlay(False)
			self.ifOverlay.hide()


	def on_close_clicked(self, button):
		self.disconnectCyborg(setResponse=True)
		print("Closing application")
		Gtk.main_quit()






"""
CSS-File integrieren
"""
cssProvider = Gtk.CssProvider()
cssProvider.load_from_path('interfaceCSS.css')
screen = Gdk.Screen.get_default()
styleContext = Gtk.StyleContext()
styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)


"""
GTK-Applikation starten
"""
win = ButtonWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
win.displayMenu()
Gtk.main()
