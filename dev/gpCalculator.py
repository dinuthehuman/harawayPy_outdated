class GpCalculator:
    def __init__(self):
        self.gpInt = False
        self.relevantChanges = 0
        self.buttonCounter = 0

    def getGpInt(self):
        return self.gpInt

    def getRelevantChanges (self):
        return self.relevantChanges

    def getButtonCounter(self):
        return self.buttonCounter

    def checkButtonClicked(self, event):
        buttonList = ["BTN_TRIGGER", "BTN_THUMB", "BTN_THUMB2", "BTN_TOP"]
        for eachButton in buttonList:
            if event.ev_type == "Key" and event.code == eachButton:
                self.relevantChanges += 1
                if event.state == 1 and self.gpInt & 2 ** self.buttonCounter == 0:
                    self.gpInt += 2 ** self.buttonCounter
                elif self.gpInt & 2 ** self.buttonCounter > 0:
                    self.gpInt -= 2 ** self.buttonCounter
                else:
                    pass
            self.buttonCounter += 1

    def CheckAxisClicked(self, event):
        axesList = ["ABS_X", "ABS_Y"]
        for eachAxe in axesList:
            if event.ev_type == "Absolute" and event.code == eachAxe:
                self.relevantChanges += 1
                if event.state < 100 and self.gpInt & 2 ** self.buttonCounter == 0:
                    self.gpInt += 2 ** self.buttonCounter
                elif event.state > 200 and self.gpInt & 2 ** (self.buttonCounter + 1) == 0:
                    self.gpInt += 2 ** (self.buttonCounter + 1)
                elif self.gpInt & (2 ** self.buttonCounter) > 0 and self.gpInt & 2 ** self.buttonCounter > 0:
                    self.gpInt -= 2 ** self.buttonCounter
                elif self.gpInt & (2 ** (self.buttonCounter + 1)) > 0 and self.gpInt & 2 ** (self.buttonCounter + 1) > 0:
                    self.gpInt -= 2 ** (self.buttonCounter + 1)
                else:
                    pass
            self.buttonCounter += 2





